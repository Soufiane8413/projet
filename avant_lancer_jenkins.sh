#!/bin/bash
mkdir /jenkins-data
chown 1000:1000 /jenkins-data
mkdir /jenkins-data/ssh
chown 1000:1000 /jenkins-data/ssh
cp /root/.ssh/id_rsa /jenkins-data/ssh/
cp /root/.ssh/id_rsa.pub /jenkins-data/ssh/
chown 1000:1000 /jenkins-data/ssh/id_rsa
chown 1000:1000 /jenkins-data/ssh/id_rsa.pub
chmod 644 /jenkins-data/ssh/id_rsa.pub
chmod 700 /jenkins-data/ssh/id_rsa

mkdir /jenkins-data/.ssh
chown 1000:1000 /jenkins-data/.ssh
#cp id_rsa /jenkins-data/.ssh/
#cp id_rsa.pub /jenkins-data/.ssh/
#chown 1000:1000 /jenkins-data/.ssh/id_rsa
#chown 1000:1000 /jenkins-data/.ssh/id_rsa.pub
#chmod 644 /jenkins-data/.ssh/id_rsa.pub
#chmod 700 /jenkins-data/.ssh/id_rsa

cp known_hosts /jenkins-data/.ssh/
chown 1000:1000 /jenkins-data/.ssh/known_hosts
chmod 644 /jenkins-data/.ssh/known_hosts
