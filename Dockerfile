FROM ubuntu:16.04
MAINTAINER S A  <test@gmail.com>

RUN apt-get update && apt-get install -y openssh-server sudo openjdk-8-jre sshpass
RUN apt-get install -y git
RUN mkdir /var/run/sshd

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
# Add a local, non-root user w/ ssh key
RUN adduser --disabled-password --shell /bin/bash --home /home/jenkinbot --gecos '' jenkinbot

RUN mkdir /home/jenkinbot/.ssh/

RUN ssh-keygen -q -t rsa -N '' -f /home/jenkinbot/.ssh/id_rsa
 
#COPY files_to_send/id_rsa /home/jenkinbot/.ssh/
#COPY files_to_send/id_rsa.pub /home/jenkinbot/.ssh/
COPY files_to_send/authorized_keys /home/jenkinbot/.ssh/authorized_keys

RUN chmod 700 /home/jenkinbot/.ssh/
RUN chmod 644 /home/jenkinbot/.ssh/authorized_keys
RUN chmod 700 /home/jenkinbot/.ssh/id_rsa
RUN chmod 644 /home/jenkinbot/.ssh/id_rsa.pub

RUN chown jenkinbot:jenkinbot /home/jenkinbot/.ssh
RUN chown jenkinbot:jenkinbot /home/jenkinbot/.ssh/authorized_keys
RUN chown jenkinbot:jenkinbot /home/jenkinbot/.ssh/id_rsa
RUN chown jenkinbot:jenkinbot /home/jenkinbot/.ssh/id_rsa.pub



RUN adduser jenkinbot sudo
RUN echo 'jenkinbot ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers


#Pour executer mon ansible
RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
RUN apt update
RUN apt install -y ansible
RUN apt-get install sshpass

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]


